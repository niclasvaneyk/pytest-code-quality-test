from pytest_gitlab_codequality_test.models import Post, User, UserPosts


def test_models_can_be_created():
    user = User(name="Test")
    post = Post(title="A Test", body="...")
    link = UserPosts(author=user, post=post, comment="Hi!")

    print(link)
